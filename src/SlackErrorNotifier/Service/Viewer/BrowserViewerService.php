<?php
namespace SlackErrorNotifier\Service\Viewer;

class BrowserViewerService implements ViewerInterface
{
    private $url = '';

    private $params = array();

    /**
     * BrowserViewerService constructor.
     * @param string $url
     * @param array $params
     */
    public function __construct($url, array $params)
    {
        $this->url = $url;
        $this->params = $params;
    }

    /**
     * @param $logId
     * @return string
     */
    public function getLink($logId)
    {
        $params = $this->params;
        foreach ($params as $key => $value) {
            if ($value == ':logId') {
                $params[$key] = $logId;
                break;
            }
        }

        return "<" . urlencode($this->url . "?" . http_build_query($params)) . "|{$logId}>";
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * @return array
     */
    public function getParams()
    {
        return $this->params;
    }

    /**
     * @param array $params
     */
    public function setParams($params)
    {
        $this->params = $params;
    }

}