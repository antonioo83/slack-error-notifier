<?php
namespace SlackErrorNotifier\Service\Transport;

class SlackApiService implements ApiInterface
{
    private $hookUrl = '';

    private $botName = '';

    private $chanel = '';

    /**
     * SlackApiService constructor.
     * @param $hookUrl
     * @param $botName
     * @param $chanel
     */
    public function __construct($hookUrl, $botName, $chanel)
    {
        $this->hookUrl = $hookUrl;
        $this->botName = $botName;
        $this->chanel = $chanel;
    }

    /**
     * Отправляет сообщение в slack.
     *
     * @param $message
     * @param string $emoji
     * @return bool
     */
    public function sendNotify($message, $emoji = '', $chanel = '')
    {
        $isSent = true;

        $fields = array(
            'channel' => empty($chanel) ? $this->chanel : $chanel,
            'username' => $this->botName,
            'text' => str_replace('&', '%26', $message),
        );
        if ($emoji) {
            $fields['icon_emoji'] = $emoji;
        }

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $this->hookUrl);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, 'payload=' . json_encode($fields));
        if (curl_exec($curl) === false) {
            $isSent = false;
        }
        curl_close($curl);

        return $isSent;
    }

}