<?php
namespace SlackErrorNotifier\Service\Transport;

use SlackErrorNotifier\Request\ErrorRequest;
use SlackErrorNotifier\Request\ExceptionRequest;
use SlackErrorNotifier\Request\LogRequest;
use SlackErrorNotifier\Service\Viewer\ViewerInterface;

class NotifierService implements NotifierInterface
{
    private $server = '';

    /**
     * @var ApiInterface
     */
    private $apiService = null;

    /**
     * @var ViewerInterface
     */
    private $viewerService = null;

    /**
     * NotifierService constructor.
     * @param string $server
     * @param ApiInterface $apiService
     * @param ViewerInterface $viewerService
     */
    public function __construct($server, ApiInterface $apiService, ViewerInterface $viewerService)
    {
        $this->server = $server;
        $this->apiService = $apiService;
        $this->viewerService = $viewerService;
    }

    /**
     * @param $message
     * @param string $emoji
     * @return mixed
     */
    public function sendMessage($chanel, $message, $emoji = '')
    {
        return $this
            ->getApiService()
            ->sendNotify($message, $emoji, $chanel);
    }

    /**
     * @param $filename
     * @param $lineNumber
     * @param $title
     * @param $description
     * @param string $emoji
     * @return mixed
     */
    public function sendErrorNotify($filename, $lineNumber, $title, $description, $emoji = ':exclamation:', $chanel = '')
    {
        $request = new ErrorRequest(
            $this->server,
            $filename,
            $lineNumber,
            $title,
            $description,
            isset($_SERVER['REQUEST_URI']) ?  $_SERVER['REQUEST_URI'] : 'Unknown request'
        );

        return $this
            ->getApiService()
            ->sendNotify($request->getMessage(), $emoji, $chanel);
    }

    /**
     * Обработчик исключений, отправляет сообщение об ошибке в мессенджер Slack.
     *
     * @param \Exception $ex
     * @param string $emoji
     * @return bool
     */
    public function sendExceptionNotify(\Exception $ex, $emoji = ':collision:', $chanel = '')
    {
        if (is_null($ex->getPrevious())) {

            return true;
        }

        $request = new ExceptionRequest(
            $this->server,
            $ex->getPrevious()->getFile(),
            $ex->getPrevious()->getLine(),
            $ex->getPrevious()->getMessage(),
            $ex->getPrevious()->getTraceAsString()
        );

        return $this
            ->getApiService()
            ->sendNotify($request->getMessage(), $emoji, $chanel);
    }

    /**
     * @param $logId
     * @param $description
     * @param $createdAt
     * @param string $emoji
     * @return mixed
     */
    public function sendLogNotify($logId, $description, $emoji = ':grey_exclamation:', $chanel = '')
    {
        $request = new LogRequest(
            $this->server,
            $description,
            ! is_null($this->getViewerService()) ? $this->getViewerService()->getLink($logId) : $logId
        );

        return $this
            ->getApiService()
            ->sendNotify($request->getMessage(), $emoji, $chanel);
    }

    /**
     * @return ViewerInterface
     */
    public function getViewerService()
    {
        return $this->viewerService;
    }

    /**
     * @return ApiInterface
     */
    public function getApiService()
    {
        return $this->apiService;
    }

}