<?php
namespace SlackErrorNotifier\Service\Transport;


interface ApiInterface
{
    public function sendNotify($text, $emoji = '', $chanel = '');
}