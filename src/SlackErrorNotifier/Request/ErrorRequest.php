<?php
namespace SlackErrorNotifier\Request;

class ErrorRequest extends BaseRequest
{
    protected $requestUri = '';

    /**
     * ErrorRequest constructor.
     * @param $server
     * @param $filename
     * @param $LineNumber
     * @param $title
     * @param $description
     * @param $requestUri
     */
    public function __construct($server, $filename, $LineNumber, $title, $description, $requestUri)
    {
        parent::__construct($server, $filename, $LineNumber, $title, $description);
        $this->requestUri = $requestUri;
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        return
            '>>>*Server:* ' . $this->server . "\r\n" .
            '*Error PHP in file*: _' . $this->filename . ' *at line* : ' . $this->lineNumber . ' _'.
            ' *with type error:* ```' . $this->title . ': ' . $this->description . '```' . "\r\n" .
            '*in* _"' . $this->requestUri . '"_';
    }

}