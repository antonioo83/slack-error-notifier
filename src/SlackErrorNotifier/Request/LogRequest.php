<?php
namespace SlackErrorNotifier\Request;

class LogRequest extends BaseRequest
{
    protected $logViewerUrl = '';

    /**
     * LogRequest constructor.
     * @param $server
     * @param $description
     * @param $logViewerUrl
     * @param $createdAt
     */
    public function __construct($server, $description, $logViewerUrl)
    {
        parent::__construct($server, '', '', '', $description);
        $this->logViewerUrl = $logViewerUrl;
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        return
            '>>>*Server:* ' . $this->server . "\r\n" .
            '*Log ID*: ' . $this->logViewerUrl . "\r\n" .
            '*Description:* _' . $this->description . '_' ;
    }

}