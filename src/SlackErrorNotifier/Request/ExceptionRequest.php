<?php
namespace SlackErrorNotifier\Request;


class ExceptionRequest extends BaseRequest
{
    /**
     * @return string
     */
    public function getMessage()
    {
        return
            '>>>*Server:* ' . $this->server . "\r\n" .
            '*Exception PHP in file*: ' . $this->filename . "\r\n" .
            '*At line* : ' . $this->lineNumber . "\r\n" .
            '*Message:* ' . $this->title . "\r\n" .
            '*Trace:* ```' . $this->description . '```';
    }
}