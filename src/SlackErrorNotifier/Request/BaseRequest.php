<?php
namespace SlackErrorNotifier\Request;

abstract class BaseRequest
{
    protected $server = '';

    protected $filename = '';

    protected $lineNumber = '';

    protected $title = '';

    protected $description = '';

    /**
     * BaseRequest constructor.
     * @param $server
     * @param $filename
     * @param $LineNumber
     * @param $title
     * @param $description
     */
    public function __construct($server, $filename, $LineNumber, $title, $description)
    {
        $this->server = $server;
        $this->filename = $filename;
        $this->lineNumber = $LineNumber;
        $this->title = $title;
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    abstract public function getMessage();

}