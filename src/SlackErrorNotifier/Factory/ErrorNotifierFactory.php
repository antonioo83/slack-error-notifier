<?php
namespace SlackErrorNotifier\Factory;

use SlackErrorNotifier\Service\Transport\NotifierService;
use SlackErrorNotifier\Service\Transport\SlackApiService;
use SlackErrorNotifier\Service\Viewer\BrowserViewerService;

class ErrorNotifierFactory implements FactoryInterface
{
    /**
     * @param $config
     * @return NotifierService|null
     */
    public static function createByConfig(array $config)
    {
        return new NotifierService(
            $config['errorHandler']['server'],
            self::getApiService($config),
            isset($config['errorHandler']['viewer']) ? self::getViewerService($config) : null
        );
    }

    /**
     * @param array $config
     * @return SlackApiService|null
     */
    private static function getApiService(array $config)
    {
        if ($config['errorHandler']['transport']['type'] == 'slack') {
            return new SlackApiService(
                $config['errorHandler']['transport']['params']['hookUrl'],
                $config['errorHandler']['transport']['params']['botName'],
                $config['errorHandler']['transport']['params']['chanel']
            );
        }

        return null;
    }

    /**
     * @param $config
     * @return BrowserViewerService|null
     */
    private static function getViewerService($config)
    {
        if ($config['errorHandler']['viewer']['type'] == 'browser') {
            return new BrowserViewerService(
                $config['errorHandler']['viewer']['params']['url'],
                $config['errorHandler']['viewer']['params']['urlParams']
            );
        }

        return null;
    }

}