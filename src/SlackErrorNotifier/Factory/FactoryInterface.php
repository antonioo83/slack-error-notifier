<?php
namespace SlackErrorNotifier\Factory;


interface FactoryInterface
{
    public static function createByConfig(array $config);
}