<?php
namespace SlackErrorNotifier\Factory;

use SlackErrorNotifier\Service\ErrorHandlerService;

class ErrorHandlerFactory implements FactoryInterface
{
    /**
     * @param array $config
     * @return ErrorHandlerService
     */
    public static function createByConfig(array $config)
    {
        return new ErrorHandlerService(
            ErrorNotifierFactory::createByConfig($config),
            isset($config['ignoreErrors']) ? $config['ignoreErrors'] : array()
        );
    }
    
}