<?php
namespace SlackErrorNotifier\Factory;

use SlackErrorNotifier\Service\Transport\SlackApiService;

class SlackNotifierFactory implements FactoryInterface
{
    /**
     * @param array $config
     * @return SlackApiService
     */
    public static function createByConfig(array $config)
    {
        return new SlackApiService(
            $config['params']['hookUrl'],
            $config['params']['botName'],
            $config['params']['chanel']
        );
    }

    /**
     * @param $hookUrl
     * @param $botName
     * @param $chanel
     * @return SlackApiService
     */
    public static function create($hookUrl, $botName, $chanel)
    {
        return new SlackApiService($hookUrl, $botName, $chanel);
    }

}