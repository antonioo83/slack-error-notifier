<?php
return array(
    'errorHandler' => array(
        'server' => 'server-name',
        'transport' => array(
            'type' => 'slack',
            'params' => array(
                'hookUrl' => 'https://hooks.slack.com',
                'botName' => 'botName',
                'chanel' => '#chanel',
            )
        ),
        // Set null if you don't want using log viewer
        'viewer' => array(
            'type' => 'browser',
            'params' => array(
                'url' => 'http://error-statistic.net/',
                'params' => array(
                    "logId" => ":logId",
                    "serverName" => "server-name"
                ),
            ),
        ),
    ),
    'ignoreErrors' => array(
        'stat()'
    ),
);